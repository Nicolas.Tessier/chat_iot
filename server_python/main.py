#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2021:
# This file is part of LEVEIL FLORIAN Enterprise, all rights reserved.

from flask import Flask
from .database import db
from flask_mqtt import Mqtt

def create_app():
    app = Flask(__name__)
    app.secret_key = b'T\xee\xc2\xc3&\xb2\xd5\x8e;\xe9\xe4B\xdc?\xbe)'
    app.config['MONGODB_SETTINGS'] = {
        'db'  : 'catdoor',
        'host': '127.0.0.1',
        'port': 27017
    }
    app.config['MQTT_BROKER_URL'] = 'broker.emqx.io'
    app.config['MQTT_BROKER_PORT'] = 1883
    app.config['MQTT_USERNAME'] = ''
    app.config['MQTT_PASSWORD'] = ''
    app.config['MQTT_KEEPALIVE'] = 5
    app.config['MQTT_TLS_ENABLED'] = False
    db.init_app(app)
    return app


app = create_app()
mqtt_client = Mqtt(app)
#app.run()

# ROUTES
from .objectsutil import routes
from .objectsutil import mqtt_utils
