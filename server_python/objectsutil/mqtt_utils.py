from flask import request, jsonify, json

from .catdoor import Catdoor
from ..main import app, mqtt_client

TOPIC_CATDOOR_STATE = "YNOVCHIOT/catdoorstate"
TOPIC_CATDOOR_PHOTO = "YNOVCHIOT/picture"
TOPIC_CATDOOR_DISTANCE = "YNOVCHIOT/distance"
TOPIC_CATDOOR_PASSAGE = "YNOVCHIOT/passage"
TOPIC_CATDOOR_CAMERA = "YNOVCHIOT/handleCamera"

def publish_message(topic, msg):
    publish_result = mqtt_client.publish(topic, msg)
    return jsonify({'code': publish_result[0]})

@mqtt_client.on_connect()
def handle_connect(client, userdata, flags, rc):
    if rc == 0:
        print('Connected successfully')
        mqtt_client.subscribe(TOPIC_CATDOOR_STATE)
        mqtt_client.subscribe(TOPIC_CATDOOR_PHOTO)
        mqtt_client.subscribe(TOPIC_CATDOOR_DISTANCE)
        mqtt_client.subscribe(TOPIC_CATDOOR_PASSAGE)
    else:
        print('Bad connection. Code:', rc)


@mqtt_client.on_message()
def handle_mqtt_message(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    topic = data["topic"]
    payload = data["payload"]
    catdoor = Catdoor().get_catdoor_by_id("1")
    if topic == TOPIC_CATDOOR_DISTANCE:
        catdoor.set_catdoor_distance(payload)
    elif topic == TOPIC_CATDOOR_STATE:
        if payload == "is_closed" or payload == "is_opened":
            catdoor.set_catdoor_is_open(payload == "is_opened")
    elif topic == TOPIC_CATDOOR_PHOTO:
        catdoor.set_catdoor_pictures(payload)
    elif topic == TOPIC_CATDOOR_PASSAGE:
        catdoor.set_catdoor_passages()
    print('Received message on topic: {topic} with payload: {payload}'.format(**data))


def post_catdoor_params(Catdoor):
    request_json = json.loads(request.data)
    Catdoor.is_open = request_json["is_open"]
    Catdoor.is_automatic_open = request_json["is_automatic_open"]
    Catdoor.is_open_action = request_json["is_open_action"]
    Catdoor.is_photo_enable = request_json["is_photo_enable"]
    Catdoor.start_hour = request_json["start_hour"]
    Catdoor.end_hour = request_json["end_hour"]
    mqtt_client.publish(TOPIC_CATDOOR_CAMERA, "enablePicture" if Catdoor.is_photo_enable else "disablePicture" )
    if Catdoor.is_automatic_open:
        mqtt_client.publish(TOPIC_CATDOOR_STATE, "automatic")
    elif Catdoor.is_open_action:
        mqtt_client.publish(TOPIC_CATDOOR_STATE, "stayOpen")
    else:
        mqtt_client.publish(TOPIC_CATDOOR_STATE, "lock")
    return Catdoor.do_save()