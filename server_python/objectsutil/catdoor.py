#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2023:
# This file is part of LeveilFlorian Enterprise, all rights reserved.

import json
import uuid
from datetime import date

from flask import jsonify, request, session

from ..database import db

TOPIC_CATDOOR_STATE = "YNOVCHIOT/catdoorstate"
TOPIC_CATDOOR_PHOTO = "YNOVCHIOT/picture"
TOPIC_CATDOOR_DISTANCE = "YNOVCHIOT/distance"
TOPIC_CATDOOR_PASSAGE = "YNOVCHIOT/passage"
TOPIC_CATDOOR_CAMERA = "YNOVCHIOT/handleCamera"


class Catdoor(db.Document):
    _id = db.StringField()
    id_of = db.IntField()
    is_open = db.BooleanField()
    is_automatic_open = db.BooleanField()
    is_open_action = db.BooleanField()
    is_photo_enable = db.BooleanField()
    start_hour = db.StringField()
    end_hour = db.StringField()
    distance = db.StringField()
    pictures = db.ListField()
    passages = db.ListField()

    def register(self):
        request_json = json.loads(request.data)
        self._id = uuid.uuid4().hex
        self.id_of = request_json["id_of"]
        self.is_open = False
        self.is_open_action = False
        self.is_automatic_open = False
        self.is_photo_enable = False
        self.start_hour = ""
        self.end_hour = ""
        self.distance = ""
        self.pictures = []
        self.passages = []

        # Check for existing id_of
        if Catdoor.objects(id_of=request_json["id_of"]):
            return jsonify({"error": "id_of address already in use"}), 400

        return self.do_save()

    def do_save(self):
        if self.save():
            return "", 200

        return jsonify({"error": "Signup failed"}), 400

    def get_catdoor_by_id(self, id_of):
        return Catdoor.objects(id_of=id_of).first()

    def get_catdoor_distance(self):
        return jsonify({"distance": self.distance}), 200

    def get_catdoor_pictures(self):
        return jsonify({"pictures": self.pictures}), 200

    def get_catdoor_passages(self):
        return jsonify({"passages": self.passages}), 200

    def set_catdoor_pictures(self, data):
        self.pictures.append(data)
        return self.do_save()

    def set_catdoor_passages(self):
        date_today = str(date.today())
        print('toto')
        for idx, passage in enumerate(self.passages):
            if passage["date"] == date_today:
                self.passages[idx]["value"] += 1
                return self.do_save()
        self.passages.append({"date" : str(date_today), "value" : 1})
        return self.do_save()

    def set_catdoor_distance(self, data):
        self.distance = data
        return self.do_save()

    def set_catdoor_is_open(self, data):
        self.is_open = data
        return self.do_save()

    def get_catdoor_params(self):
        return jsonify({"is_open": self.is_open,
                        "is_automatic_open": self.is_automatic_open,
                        "is_open_action": self.is_open_action,
                        "is_photo_enable": self.is_photo_enable,
                        "start_hour": self.start_hour,
                        "end_hour": self.end_hour}), 200

