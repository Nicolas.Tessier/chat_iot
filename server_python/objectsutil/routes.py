#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2021:
# This file is part of LeveilFlorian Enterprise, all rights reserved.
from .mqtt_utils import post_catdoor_params
from ..main import app, mqtt_client
from .catdoor import Catdoor
from flask import request, jsonify


@app.route('/catdoor/register', methods=['POST'])
def register():
    return Catdoor().register()


@app.route('/catdoor/distance')
def get_my_distance():
    catdoor = Catdoor().get_catdoor_by_id(request.args.get('catdoor_id', default='', type=str))
    return catdoor.get_catdoor_distance()


@app.route('/catdoor/params')
def get_my_params():
    print(request.args.get('catdoor_id', default='', type=str))
    catdoor = Catdoor().get_catdoor_by_id(request.args.get('catdoor_id', default='', type=str))
    return catdoor.get_catdoor_params()

@app.route('/catdoor/params', methods=['POST'])
def post_my_params():
    catdoor = Catdoor().get_catdoor_by_id(request.args.get('catdoor_id', default='', type=str))
    return post_catdoor_params(catdoor)


@app.route('/catdoor/pictures')
def get_my_pictures():
    print(request.args.get('catdoor_id', default='', type=str))
    catdoor = Catdoor().get_catdoor_by_id(request.args.get('catdoor_id', default='', type=str))
    return catdoor.get_catdoor_pictures()


@app.route('/catdoor/passages')
def get_my_passages():
    catdoor = Catdoor().get_catdoor_by_id(request.args.get('catdoor_id', default='', type=str))
    return catdoor.get_catdoor_passages()
