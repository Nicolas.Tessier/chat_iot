#include <Arduino.h>
#include "EspMQTTClient.h"

#define CHAT

// MQQT SERVER //
EspMQTTClient client(
    "Nico",           // ssid
    "c'est bon ?",    // password
    "broker.emqx.io", // mqtt broker adress
#if defined(CAM)
    "esp32-65148974" // user id
#else
    "esp32-9845659" // user id
#endif
);
#if defined(CAM)

// LIBRARIES FOR PICTURE AND TO ENCODE //
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "Base64.h"
#include "esp_camera.h"

// DEFINE CAMERA MODEL TO IMPORT RIGHT CONFIG //
#define CAMERA_MODEL_WROVER_KIT
#include "camera_pins.h"

void sendImage()
{
  camera_fb_t *fb = NULL;
  // take picture
  fb = esp_camera_fb_get();
  if (!fb)
  {
    Serial.println("Camera capture failed");
  }
  else
  {
    Serial.println("Camera capture success");
  }

  // format & encode picture
  char *input = (char *)fb->buf;
  char output[base64_enc_len(3)];
  String imageFile = "data:image/jpeg;base64,";
  for (int i = 0; i < fb->len; i++)
  {
    base64_encode(output, (input++), 3);
    if (i % 3 == 0)
      imageFile += String(output);
  }

  // send picture
  client.publish("YNOVCHIOT/picture", imageFile);

  esp_camera_fb_return(fb);
}

void initCamera()
{
  // CAMERA CONFIG //
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  config.fb_count = 2;
  config.grab_mode = CAMERA_GRAB_LATEST;
  config.frame_size = FRAMESIZE_QVGA;
  config.jpeg_quality = 12;

  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK)
  {
    Serial.printf("Camera init failed with error 0x%x", err);
    ESP.restart();
  }

  sensor_t *s = esp_camera_sensor_get();
  if (s->id.PID == OV3660_PID)
  {
    s->set_vflip(s, 1);
    s->set_brightness(s, 1);
    s->set_saturation(s, -2);
  }
  s->set_framesize(s, FRAMESIZE_QVGA); // QVGA(320x240)
}

#elif defined(CHAT)

// BLUETOOTH IMPORT //
#include <ESP32Servo.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

// BLUETOOTH //
int scanTime = 1;
float distanceBetweenCatAndCatDoor;
BLEScan *pBLEScan;
TaskHandle_t BluetoothTask;

// SERVO MOTOR //
int POS_OPEN = 90;
int POS_CLOSE = 0;
int pos = POS_CLOSE;
int servoPin = 33;
Servo myservo;
bool lock = false;
bool stayOpen = false;
bool enablePicture = true;

// LIMIT SWITCH
int is_closed_cat_door = 19;

void catPassage()
{
  if (!lock)
  {
    client.publish("YNOVCHIOT/passage", "gone");

    if (enablePicture)
    {
      // ask for picture
      client.publish("YNOVCHIOT/handleCamera", "need_picture");
    }
  }
}

void doOpenOrClosePetDoor(bool closed)
{
  // if lock or stay open enable by user, no action possible
  if (lock || stayOpen)
  {
    return;
  }

  bool do_close = closed;
  bool is_open = (pos == POS_OPEN);
  if ((is_open && do_close) || (!is_open && !do_close))
  {
    // close
    if (do_close)
    {
      pos = POS_CLOSE;
      myservo.write(POS_CLOSE);
      client.publish("YNOVCHIOT/catdoorstate", "is_closed");
    }
    // open
    else
    {
      pos = POS_OPEN;
      myservo.write(POS_OPEN);
      client.publish("YNOVCHIOT/catdoorstate", "is_opened");
    }
  }
}

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks
{
  void onResult(BLEAdvertisedDevice advertisedDevice)
  {
    if (advertisedDevice.getName() == "iPhoneflev")
    {
      distanceBetweenCatAndCatDoor = pow(10, ((float(-69 - (advertisedDevice.getRSSI()))) / (float(10 * 2))));
      if (distanceBetweenCatAndCatDoor <= 0.35)
      {
        doOpenOrClosePetDoor(false);
      }
      else
      {
        if (digitalRead(is_closed_cat_door))
        {
          doOpenOrClosePetDoor(true);
        }
      }
      // Serial.println(" Distance between cat and cat door = " + String(distanceBetweenCatAndCatDoor, 5)); // DEBUG print stats of bluetooth
      client.publish("YNOVCHIOT/distance", String(distanceBetweenCatAndCatDoor));
    }
    else
      distanceBetweenCatAndCatDoor = 10;
  }
};

// BLE Scan
void Task1code(void *pvParameters)
{
  for (;;)
  {
    // =========================================== BLUETOOTH       =========================================== //
    BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
    for (int i = 0; i < foundDevices.getCount(); i++)
    {
      BLEAdvertisedDevice device = foundDevices.getDevice(i);
    }
    pBLEScan->clearResults();

    delay(100);
  }
}

#endif

void onConnectionEstablished()
{
  Serial.println("connection establish");

#if defined(CAM)
  // take and send picture
  client.subscribe("YNOVCHIOT/handleCamera", [](const String &payload)
                   {
        if (payload == "need_picture") {

      sendImage();
    } });

#elif defined(CHAT)
  // take and send picture
  client.subscribe("YNOVCHIOT/handleCamera", [](const String &payload)
                   {
    if (payload == "enablePicture") {
      enablePicture = true;
    }
    if (payload == "disablePicture") {
      enablePicture = false;
    } });

  client.subscribe("YNOVCHIOT/catdoorstate", [](const String &payload)
                   {
    // lock until unlock
    if (payload == "lock") {
      stayOpen = false;
      doOpenOrClosePetDoor(true);
      lock = true;
    }
    // cat door always open
    if (payload == "stayOpen") {
      lock = false;
      doOpenOrClosePetDoor(false);
      stayOpen = true;
    }
    // default behaviour
    if (payload == "automatic") {
      stayOpen = false;
      lock = false;
      doOpenOrClosePetDoor(true);
    }
    // open catdoor
    if (payload == "do_open") {
      doOpenOrClosePetDoor(false);
    }
    // close catdoor
    if (payload == "do_close") {
      doOpenOrClosePetDoor(true);
    } });
#endif
  client.subscribe("YNOVCHIOT/esp32", [](const String &payload)
                   {
          if (payload == "ping") {
      client.publish("YNOVCHIOT/esp32", "pong");
    } });
}

void setup()
{
  Serial.begin(115200);

#if defined(CAM)
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  initCamera();
  client.setMaxPacketSize(20000);

#elif defined(CHAT)
  // BLUETOOTH
  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);

  // Loop for bluetooth
  xTaskCreatePinnedToCore(
      Task1code,
      "BluetoothTask",
      10000,
      NULL,
      1,
      &BluetoothTask,
      1);

  // SERVO MOTOR
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  myservo.setPeriodHertz(50);
  myservo.attach(servoPin, 500, 2400);
  myservo.write(pos);
#endif
}

void loop()
{
  // =========================================== MQTT CONNECTION =========================================== //
  client.loop();

#if defined(CHAT)
  // =========================================== CATDOOR OPENNED =========================================== //
  if (!digitalRead(is_closed_cat_door))
  {
    catPassage();
    delay(500);
  }
#endif
}
