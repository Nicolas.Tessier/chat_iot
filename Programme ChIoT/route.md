## YNOVCHIOT/picture

publication de la photo par l'esp32 cam  
*contenu : image en base64*

## YNOVCHIOT/passage

indication du passage du chat par l'esp32 chatière  
*contenu : "gone"*


## YNOVCHIOT/handleCamera

demande de photo par l'esp32 chatière  
*contenu : "need_picture"*

activation de la photo par l'API
*contenu : "enablePicture"*

désactivation de la photo par l'API
*contenu : "disablePicture"*

## YNOVCHIOT/catdoorstate

indication de fermeture de la chatière par l'esp32 chatière   
*contenu : "is_closed"*

indication de l'ouverture de la chatière par l'esp32 chatière   
*contenu : "is_opened"*

bloquage permanent de la chatière par l'API  
*contenu : "lock"*

demande d'ouverture permanente de la chatière par l'API  
*contenu : "stayOpen"*  

comportement par défaut de la chatière par l'API  
*contenu : "automatic"*

## YNOVCHIOT/distance

valeur de la distance entre le chat et la chatière par l'esp32 chatière   
*contenu : distance chat-chatière*
