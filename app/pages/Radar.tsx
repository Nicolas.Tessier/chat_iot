import {
  Text,
  View,
  Button,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import CircularProgress from "react-native-circular-progress-indicator";
import axios from "axios";
import API_URL from "../urlAPI";

function Radar() {
  const navigation = useNavigation();

  const [distance, setDistance] = useState(0.3);

  const getDistance = () => {
    axios
      .get(`${API_URL}distance?catdoor_id=1`)
      .then((response) => {
        console.log("get radar");
        setDistance(parseFloat(response.data.distance));
      })
      .catch((e) => {
        console.log("marche po radar");
      });
  };

  useEffect(() => {
    getDistance();
  }, []);

  return (
    <View style={styles.screen}>
      <CircularProgress
        value={distance}
        radius={120}
        activeStrokeColor={"gray"}
        inActiveStrokeColor={"#007FFF"}
        maxValue={1}
        progressValueColor={"#007FFF"}
        title={`mètre${distance == 0 ? "" : "s"}`}
        titleColor={"#007FFF"}
        titleStyle={{ fontWeight: "bold" }}
        progressFormatter={(value: number) => {
          "worklet";
          return value.toFixed(2); // 2 decimal places
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Radar;
