/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import { useNavigation } from "@react-navigation/native";
import React from "react";
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
} from "react-native";
import { FlatGrid } from "react-native-super-grid";

import { Colors } from "react-native/Libraries/NewAppScreen";

const Home = () => {
  const isDarkMode = useColorScheme() === "dark";
  let backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  const navigation = useNavigation();
  return (
    <FlatGrid
      style={{
        marginLeft: 10,
        marginRight: 10,
      }}
      itemDimension={130}
      data={[
        { name: "Activités", color: "#27187E", path: "Activity" },
        { name: "Chatière", color: "#758BFD", path: "CatFlap" },
        { name: "Caméra", color: "#AEB8FE", path: "Camera" },
        { name: "Images", color: "#FF8600", path: "Gallery" },
        { name: "Radar", color: "#758BFD", path: "Radar" },
        { name: "Paramètres", color: "#27187E", path: "Parameters" },
      ]}
      renderItem={({
        item,
      }: {
        item: { name: string; color: string; path: string };
      }) => (
        <TouchableOpacity onPress={() => navigation.navigate(item.path)}>
          <Text style={[styles.buttonView, { backgroundColor: item.color }]}>
            {item.name}
          </Text>
        </TouchableOpacity>
      )}
    />
  );
};

const styles = StyleSheet.create({
  buttonView: {
    textAlignVertical: "center",
    textAlign: "center",
    borderRadius: 10,
    fontSize: 20,
    color: "white",
    width: 175,
    height: 150,
  },
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Home;
