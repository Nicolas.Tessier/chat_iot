import { Text, View, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import * as Progress from "react-native-progress";
import { SimpleGrid } from "react-native-super-grid";
import { List } from "react-native-paper";
import axios from "axios";
import API_URL from "../urlAPI";
export interface IDays {
  date: string;
  value: number;
}
function Activity() {
  const navigation = useNavigation();
  const [progress, setProgress] = useState(0);
  const [days, setDays] = useState<IDays[]>([]);

  const getPassages = () => {
    axios.get(`${API_URL}passages?catdoor_id=1`).then((response) => {
      const last7Days = response.data.passages.slice(-7);
      setDays(last7Days);
    });
  };

  useEffect(() => {
    getPassages();
  }, []);

  const maxValue = Math.max(...days.map((x) => x.value));

  return (
    <View style={styles.screen}>
      <SimpleGrid
        itemDimension={30}
        data={days}
        maxItemsPerRow={8}
        adjustGridToStyles={true}
        renderItem={({ item, index }) => {
          return (
            <View style={styles.test}>
              <Progress.Bar
                progress={item.value / maxValue}
                style={[
                  { transform: [{ rotate: "-90deg" }] },
                  { borderRadius: 10 },
                ]}
                color={index % 2 == 0 ? "#017ffe" : "#f09aff"}
                height={20}
              />
              <Text
                style={{
                  color: "#3c3c43",
                  opacity: 0.6,
                  fontSize: 11,
                  marginVertical: 75,
                }}
              >
                {`${
                  new Date(item.date).getDate() <= 9
                    ? `0${new Date(item.date).getDate()}`
                    : new Date(item.date).getDate()
                }/${
                  new Date(item.date).getMonth() + 1 <= 9
                    ? `0${new Date(item.date).getMonth() + 1}`
                    : new Date(item.date).getDate() + 1
                }`}
              </Text>
            </View>
          );
        }}
      />
      <Text>Activités</Text>
      <SimpleGrid
        itemDimension={26}
        data={days}
        maxItemsPerRow={1}
        adjustGridToStyles={true}
        renderItem={({ item, index }) => {
          return (
            <View
              style={{
                paddingVertical: 5,
                paddingHorizontal: 10,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <List.Icon
                color={index % 2 == 0 ? "#017ffe" : "#f09aff"}
                icon="circle"
              />
              <Text
                style={{
                  fontSize: 16,
                  color: "black",
                }}
              >
                {new Date(item.date).toLocaleDateString("fr-FR", {
                  weekday: "short",
                  month: "short",
                  day: "numeric",
                })}
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  color: "black",
                }}
              >
                {item.value}
              </Text>
            </View>
          );
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  buttonView: {
    textAlignVertical: "center",
    textAlign: "center",
    borderRadius: 10,
    fontSize: 20,
    color: "white",
    width: 175,
    height: 150,
  },
  screen: {
    flex: 1,
    alignItems: "center",
    margin: 100,
  },
  test: {
    alignItems: "center",
  },
});

export default Activity;
