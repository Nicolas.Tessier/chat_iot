import { Text, View, Button, Touchable, Modal, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import CardImage from "../components/CardImage";
import { FlatGrid } from "react-native-super-grid";
import { TouchableOpacity } from "react-native-gesture-handler";
import ImageFullscreen from "../components/ImageFullscreen";
import axios from "axios";
import API_URL from "../urlAPI";

function Gallery() {
  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  const [activeItem, setActiveItem] = useState("");
  const [pictures, setPictures] = useState([]);

  const getPictures = () => {
    axios
      .get(`${API_URL}pictures?catdoor_id=1`)
      .then((response) => {
        console.log("get pictures");

        setPictures(response.data.pictures);
      })
      .catch((e) => {
        console.log("marche po radar");
      });
  };

  useEffect(() => {
    getPictures();
  }, []);

  const onPress = (item: string) => {
    setActiveItem(item);
    setModalVisible(true);
  };

  return (
    <View>
      <FlatGrid
        style={{
          marginLeft: 10,
          marginRight: 10,
        }}
        itemDimension={130}
        data={pictures}
        renderItem={({ item, index }: { item: string; index: number }) => (
          <TouchableOpacity onPress={() => onPress(item)}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                setModalVisible(!modalVisible);
              }}
            >
              <ImageFullscreen img={activeItem} />
            </Modal>
            <CardImage img={item} />
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

export default Gallery;
