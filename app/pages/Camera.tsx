import { Text, View, Button, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import React from "react";

function Camera() {
  const navigation = useNavigation();

  return (
    <View style={styles.screen}>
      <Text style={{ fontSize: 50 }}>🚧 WIP 🚧</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Camera;
