import {
  Text,
  View,
  Button,
  Switch,
  TouchableOpacity,
  StyleSheet,
  Alert,
  ToastAndroid,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Divider } from "@rneui/themed";
import DatePicker from "react-native-date-picker";
import { FlatGrid } from "react-native-super-grid";
import axios from "axios";
import API_URL from "../urlAPI";

function CatFlap() {
  const navigation = useNavigation();

  const [isOpen, setIsOpen] = useState(false);
  const toggleSwitchIsOpen = () => {
    setIsOpenAction(!isOpenAction);
  };

  const [isOpenAutoEnable, setIsOpenAutoEnable] = useState(false);
  const toggleSwitchIsOpenAutoEnable = () => {
    setIsOpenAutoEnable(!isOpenAutoEnable);
  };

  const [isPhotoEnable, setIsPhotoEnable] = useState(false);
  const toggleSwitchIsPhotoEnable = () => {
    setIsPhotoEnable(!isPhotoEnable);
  };

  const [isOpenAction, setIsOpenAction] = useState(false);
  const [isClassic, setClassic] = useState(true);
  const [timeStartOpen, setTimeStartOpen] = useState(false);
  const [timeEndOpen, setTimeEndOpen] = useState(false);
  const [timeStart, setTimeStart] = useState(new Date(1, 1, 1, 8, 0));
  const [timeEnd, setTimeEnd] = useState(new Date(1, 1, 1, 18, 0));

  const postData = () => {
    console.log({
      is_automatic_open: isOpenAutoEnable,
      is_open: isOpen,
      is_photo_enable: isPhotoEnable,
      is_open_action: isOpenAction,
      start_hour: `${
        timeStart.getUTCHours() <= 9
          ? `0${timeStart.getUTCHours()}`
          : timeStart.getUTCHours()
      }h${
        timeStart.getUTCMinutes() <= 9
          ? `0${timeStart.getUTCMinutes()}`
          : timeStart.getUTCMinutes()
      }`,
      end_hour: `${
        timeEnd.getHours() <= 9 ? `0${timeEnd.getHours()}` : timeEnd.getHours()
      }h${
        timeEnd.getMinutes() <= 9
          ? `0${timeEnd.getMinutes()}`
          : timeEnd.getMinutes()
      }`,
    });

    const options = {
      url: `${API_URL}params?catdoor_id=1`,
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
      },
      data: {
        is_automatic_open: isOpenAutoEnable,
        is_open: isOpen,
        is_photo_enable: isPhotoEnable,
        is_open_action: isOpen,
        start_hour: `${
          timeStart.getHours() <= 9
            ? `0${timeStart.getHours()}`
            : timeStart.getHours()
        }h${
          timeStart.getMinutes() <= 9
            ? `0${timeStart.getMinutes()}`
            : timeStart.getMinutes()
        }`,
        end_hour: `${
          timeEnd.getHours() <= 9
            ? `0${timeEnd.getHours()}`
            : timeEnd.getHours()
        }h${
          timeEnd.getMinutes() <= 9
            ? `0${timeEnd.getMinutes()}`
            : timeEnd.getMinutes()
        }`,
      },
    };
    axios(options).then((response) => {});
  };

  useEffect(() => {
    axios
      .get(`${API_URL}params?catdoor_id=1`)
      .then((response) => {
        console.log("call");
        setTimeStart(
          response.data.start_hour == ""
            ? timeStart
            : new Date(
                1,
                1,
                1,
                parseInt(response.data.start_hour.split("h")[0]),
                parseInt(response.data.start_hour.split("h")[1])
              )
        );
        setTimeEnd(
          response.data.end_hour == ""
            ? timeEnd
            : new Date(
                1,
                1,
                1,
                parseInt(response.data.end_hour.split("h")[0]),
                parseInt(response.data.end_hour.split("h")[1])
              )
        );
        setIsOpenAction(response.data.is_open_action);
        setIsPhotoEnable(response.data.is_photo_enable);
        setIsOpenAutoEnable(response.data.is_automatic_open);
      })
      .catch(() => {
        console.log("api ko");
      });
  }, []);

  useEffect(() => {
    postData();
  }, [
    isOpen,
    isOpenAutoEnable,
    isPhotoEnable,
    timeStart,
    timeEnd,
    isOpenAction,
  ]);

  function onPressClassic() {
    setClassic(isClassic ? false : true);
  }

  return (
    <View>
      <View style={{ marginLeft: 20, marginTop: 20, marginBottom: 20 }}>
        <Text style={styles.greyTextBold}>
          Chatière {isOpen ? "ouverte" : "fermée"}
        </Text>
        <Text style={styles.greyTextBold}>
          État Actuel: {isOpen ? "Ouvert" : "Fermé"}
        </Text>
        <Text style={styles.greyTextBold}>
          Mode utilisé: {isClassic ? "Classique" : "Avancé"}
        </Text>
      </View>

      <View>
        <View style={styles.container}>
          <TouchableOpacity
            style={[
              styles.btn,
              isClassic ? { elevation: 10 } : { elevation: 0 },
            ]}
            disabled={isClassic}
            onPress={onPressClassic}
          >
            <Text>Classique</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.btn,
              !isClassic ? { elevation: 10 } : { elevation: 0 },
            ]}
            disabled={!isClassic}
            onPress={onPressClassic}
          >
            <Text>Avancé</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View>
        <View style={styles.classic}>
          <View>
            {isClassic ? (
              <View>
                <Text style={styles.blackText}>
                  {isOpenAction ? "Fermer" : "Ouvrir"}
                </Text>
                <Text style={styles.greyText}>
                  {isOpenAction ? "Fermer" : "Ouvrir"} la chatière
                </Text>
              </View>
            ) : (
              <View>
                <Text style={styles.blackText}>Ouvrir</Text>
                <FlatGrid
                  itemDimension={60}
                  showsVerticalScrollIndicator={false}
                  data={[
                    {
                      time: timeStart,
                      state: timeStartOpen,
                      fun: setTimeStart,
                      funOpen: setTimeStartOpen,
                    },
                    {
                      time: timeEnd,
                      state: timeEndOpen,
                      fun: setTimeEnd,
                      funOpen: setTimeEndOpen,
                    },
                  ]}
                  renderItem={({
                    item,
                    index,
                  }: {
                    item: {
                      time: Date;
                      state: boolean;
                      fun: Function;
                      funOpen: Function;
                    };
                    index: number;
                  }) => (
                    <View>
                      <TouchableOpacity
                        style={styles.btnHours}
                        onPress={() => item.funOpen(true)}
                      >
                        <Text style={{ fontSize: 16 }}>{`${
                          item.time.getHours() <= 9
                            ? `0${item.time.getHours()}`
                            : item.time.getHours()
                        }h${
                          item.time.getMinutes() <= 9
                            ? `0${item.time.getMinutes()}`
                            : item.time.getMinutes()
                        }`}</Text>
                      </TouchableOpacity>
                      <DatePicker
                        modal
                        open={item.state}
                        date={item.time}
                        timeZoneOffsetInMinutes={60}
                        onConfirm={(date) => {
                          item.funOpen(false);
                          item.fun(date);
                        }}
                        mode="time"
                        onCancel={() => {
                          item.funOpen(false);
                        }}
                      />
                      <Text
                        style={{
                          fontSize: 16,
                          marginLeft: 20,
                        }}
                      >
                        {index == 0 ? "Début" : "Fin"}
                      </Text>
                    </View>
                  )}
                />
              </View>
            )}
          </View>
          <Switch
            onValueChange={() => toggleSwitchIsOpen()}
            value={isOpenAction}
            thumbColor={isOpenAction ? "#017ffe" : "#949494"}
            trackColor={{ true: "#90c7fe", false: "#d3d3d3" }}
          />
        </View>

        <Divider style={{ marginLeft: 15, marginRight: 15, marginTop: 15 }} />

        <View style={styles.classic}>
          <View>
            <Text style={styles.blackText}>Ouverture automatique</Text>
            <Text style={styles.greyText}>
              Ouvrir automatiquement la chatière quand le chat {"\n"} est à
              moins d’un mètre même si elle est fermée
            </Text>
          </View>
          <Switch
            onValueChange={() => toggleSwitchIsOpenAutoEnable()}
            value={isOpenAutoEnable}
            thumbColor={isOpenAutoEnable ? "#017ffe" : "#949494"}
            trackColor={{ true: "#90c7fe", false: "#d3d3d3" }}
          />
        </View>

        <Divider style={{ marginLeft: 15, marginRight: 15, marginTop: 15 }} />

        <View style={styles.classic}>
          <View>
            <Text style={styles.blackText}>Photo</Text>
            <Text style={styles.greyText}>
              Prendre une photographie lorsque que le chat {"\n"} passe dedans
            </Text>
          </View>
          <Switch
            onValueChange={() => toggleSwitchIsPhotoEnable()}
            value={isPhotoEnable}
            thumbColor={isPhotoEnable ? "#017ffe" : "#949494"}
            trackColor={{ true: "#90c7fe", false: "#d3d3d3" }}
          />
        </View>
        <Divider style={{ marginLeft: 15, marginRight: 15, marginTop: 15 }} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  greyTextBold: {
    fontWeight: "bold",
    fontSize: 16,
    marginBottom: 5,
  },

  greyText: {
    color: "#3c3c43",
    opacity: 0.6,
    fontSize: 12,
  },

  blackText: {
    fontSize: 17,
    color: "#000000",
  },
  textHours: {
    alignContent: "center",
    alignItems: "center",
  },
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F8FAFD",
    alignSelf: "center",
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
  },
  btn: {
    overflow: "visible",
    shadowColor: "#52006A",
    backgroundColor: "white",
    width: 150,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginRight: 5,
    marginTop: 3,
    marginBottom: 3,
    marginLeft: 3,
  },
  btnHours: {
    shadowColor: "#52006A",
    backgroundColor: "white",
    width: 71,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    shadowOffset: { width: -2, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginRight: 5,
    marginTop: 3,
    marginBottom: 3,
    marginLeft: 3,
  },
  classic: {
    marginTop: 20,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
  },
});

export default CatFlap;
