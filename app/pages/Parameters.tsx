import { View, Button, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Text } from "react-native-paper";

function Parameters() {
  const navigation = useNavigation();

  return (
    <>
      <View style={styles.screen}>
        <Text style={styles.test} variant="headlineLarge">
          John Doe
        </Text>
        <Text style={[styles.mail, styles.test]} variant="bodyLarge">
          john.doe@gmail.com
        </Text>
        <Text style={styles.test} variant="bodyLarge">
          asbr-as32-3sd2-az54x-mpz5
        </Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonView}>Changement de mail</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonView}>Changement de mot de passe</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonView}>
            Changement d'identifiant de l'appareil
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.lastButton}>
          <Text style={styles.buttonView}>Déconnexion </Text>
        </TouchableOpacity>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: "flex-start",
    marginLeft: 10,
    marginRight: 10,
  },
  mail: {
    textDecorationLine: "underline",
  },
  buttonView: {
    textAlignVertical: "center",
    borderRadius: 5,
    fontSize: 20,
    color: "black",
    height: 50,
    marginLeft: 10,
  },
  button: {
    width: "100%",
    borderColor: "lightgray",
    borderRadius: 1,
    margin: 3,
    borderBottomWidth: 1,
  },
  lastButton: {
    width: "100%",
    borderColor: "lightgray",
    borderRadius: 1,
    margin: 3,
  },
  test: {
    margin: 10,
  },
});

export default Parameters;
