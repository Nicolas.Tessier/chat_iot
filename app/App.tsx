import { createStackNavigator } from "@react-navigation/stack";
import Home from "./pages/Home";
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
} from "@react-navigation/native";
import Camera from "./pages/Camera";
import CatFlap from "./pages/CatFlap";
import Activity from "./pages/Activity";
import Radar from "./pages/Radar";
import Gallery from "./pages/Gallery";
import Parameters from "./pages/Parameters";
import React from "react";
import { Alert, Button, useColorScheme } from "react-native";
import ImageFullscreen from "./components/ImageFullscreen";

type RootStackParamList = {
  Activity: undefined;
  CatFlap: undefined;
  Camera: undefined;
  Gallery: undefined;
  Parameters: undefined;
  Radar: undefined;
  Home: undefined;
  ImageFullscreen: { userId: string };
};

const Stack = createStackNavigator<RootStackParamList>();

function App() {
  const scheme = useColorScheme();
  return (
    <NavigationContainer theme={scheme === "dark" ? DarkTheme : DefaultTheme}>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Activity"
          component={Activity}
          options={{
            title: "Activités",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="CatFlap"
          component={CatFlap}
          options={{
            title: "Chatière",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Camera"
          component={Camera}
          options={{
            title: "Caméra",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Gallery"
          component={Gallery}
          options={{
            title: "Galerie",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: "Accueil",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Radar"
          component={Radar}
          options={{
            title: "Radar",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen
          name="Parameters"
          component={Parameters}
          options={{
            title: "Paramètres",
            headerTitleAlign: "center",
          }}
        />
        <Stack.Screen name="ImageFullscreen" component={ImageFullscreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
