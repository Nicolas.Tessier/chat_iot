import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View, Image, StyleSheet } from "react-native";
import { Avatar, Button, Card, Text } from "react-native-paper";

const ImageFullscreen = (props: { img: string }) => {
  return (
    <View>
      <Card>
        <Card.Cover
          source={{
            uri: props.img,
          }}
          style={styles.imgFullscreen}
        />
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  imgFullscreen: {
    width: "100%",
    height: "100%",
  },
});

export default ImageFullscreen;
