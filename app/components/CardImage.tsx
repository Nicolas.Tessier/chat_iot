import React from "react";
import { View, Image } from "react-native";
import { Avatar, Button, Card, Text } from "react-native-paper";

const CardImage = (props: { img: string }) => {
  return (
    <View>
      <Card>
        <Card.Cover source={{ uri: props.img }} />
      </Card>
    </View>
  );
};

export default CardImage;
